/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */

#include "NAME_TO_BE_REPLACED.h"

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>()
{
  //TODO: inherit from an appropriate postprocessor
  InputParameters params = validParams<ElementIntegralPostprocessor>();

  return params;
}

NAME_TO_BE_REPLACED::NAME_TO_BE_REPLACED(const std::string & name, InputParameters parameters) :
    ElementIntegralPostprocessor(name, parameters)
{
}

Real
NAME_TO_BE_REPLACED::computeQpIntegral()
{
  //return 1.0;
}
