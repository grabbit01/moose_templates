/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */

#ifndef NAME_TO_BE_REPLACED_H
#define NAME_TO_BE_REPLACED_H

//TODO: include the base header
#include "ElementIntegralPostprocessor.h"

//Forward Declarations
class NAME_TO_BE_REPLACED;

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>();

//TODO: change the base class!
class NAME_TO_BE_REPLACED : public ElementIntegralPostprocessor
{
public:
  NAME_TO_BE_REPLACED(const std::string & name, InputParameters parameters);

protected:
  virtual Real computeQpIntegral();
};

#endif
