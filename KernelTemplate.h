/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */

#ifndef NAME_TO_BE_REPLACED_H
#define NAME_TO_BE_REPLACED_H

#include "Kernel.h"

//Forward Declarations
class NAME_TO_BE_REPLACED;

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>();

class NAME_TO_BE_REPLACED: public Kernel
{
public:

  NAME_TO_BE_REPLACED(const std::string & name, InputParameters parameters);

protected:
  virtual Real computeQpResidual();

  virtual Real computeQpJacobian();

  virtual Real computeQpOffDiagJacobian(unsigned int jvar);

private:

  //const bool _xdisp_coupled;
  //const unsigned int _xdisp_var;

};
#endif //NAME_TO_BE_REPLACED_H
