[Mesh]
  #file = mug.e
  type=GeneratedMesh
  dim=2
  nx=3
  ny=3
  xmin=0.0
  xmax=1.0
  ymin=0.0
  ymax=1.0
  #uniform_refine=1

[]

[Variables]
  #active = 'NAME'
  [./CHANGEME]
    order = FIRST
    family = LAGRANGE
  [../]
[]


[AuxVariables]
  [./CHANGEME]
    order = FIRST
    family = LAGRANGE
  [../]
[]

[Materials]
  [./CHANGEME]
     type=
     block=
  [../]
[]

[Functions]
  [./CHANGEME]
      type=SolutionFunction
      file_type=exodusII
      mesh=       #file name like: in.e
      variable=   #the variable in the file to be read in
      timestep=   #the timestep to be read in.
  [../]
[]

[Kernels]
  #active = 'diff_convected conv diff_diffused'
  [./CHANGEME]
    type = Convection
    variable = convected

    # Couple a variable into the convection kernel using local_name = simulationg_name syntax
    some_variable = diffused
  [../]
[]

[AuxKernels]
  #active='diff'
  [./CHANGEME]
    type =
    variable =
  [../]
[] # AuxKernels

[BCs]
  #active = 'bottom_convected top_convected bottom_diffused top_diffused'
  [./CHANGEME]
    type = DirichletBC
    variable = convected
    boundary = 'bottom'
    value = 1
  [../]
[]

[ICs]
  [./CHANGEME]
    type=RandomIC
    variable=
  [../]
[]

[Executioner]
  type = Steady
  petsc_options = '-snes_mf_operator'
[]

[Eigen]
   nev = 10      #number of requested eigen vector
   #ncv=max(2*nev,nev+15)         #the largest dimension of working subspace
   #maxits =    #maximum number of iteration, default 10000
   #tol = 1e-6   #tolerace,
   #scaling_factor=1     #scaling factor due to measure unit, the computed eigen value will be multiplied by this number before computing frequency
   #file_base            #the base of output filenames
   #export = false    #if true, the ith eigenvectors are saved to  file_base_i.e,
   #animation = true       #if true, the ith vibration will be genarated and save to file file_base_all_i.e
   #num_sample=32          #the number of sample in one period in the animation
   slepc_options='-eps_smallest_magnitude -eps_view'
   slepc_options_iname='-st_type -st_shift   -eps_type  -st_ksp_type -st_pc_type'
   slepc_options_value='sinvert     0.0        arnoldi     preonly        lu'
[]

[Preconditioning]
   [./CHANGEME]
     type=FDP   #or SMP
     #off_diag_row='var_name'
     #off_diag_column='var_name'
     #full=true   #to use every off diagonal block
     #petsc_options='snes_mf_operator'
     #petsc_options_iname = '-pc_type -mat_fd_coloring_err -mat_fd_type'
     #petsc_options_value = 'lu       1e-6                 ds'
   [../]
[]

[Postprocessors]
  [./CHANGEME]
     type=
  [../]
[]

[Output]
  file_base = out
  interval = 1
  elemental_as_nodal=true
  exodus = true
  perf_log = true
[]
