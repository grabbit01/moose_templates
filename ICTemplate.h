/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */
#ifndef NAME_TO_BE_REPLACED_H
#define NAME_TO_BE_REPLACED_H

#include "Kernel.h"
#include "InitialCondition.h"
#include "InputParameters.h"

// Forward Declarations
class NAME_TO_BE_REPLACED;

namespace libMesh { class Point; }

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>();


class NAME_TO_BE_REPLACED:public InitialCondition
{
public:
  NAME_TO_BE_REPLACED(const std::string & name, InputParameters parameters);

  /**
   * The value of the variable at a point.
   *
   * This must be overriden by derived classes.
   */
  virtual Real value(const Point & p);
};

#endif //NAME_TO_BE_REPLACED
