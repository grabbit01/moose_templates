/**
 * @file
 * @author S. Gu <sgu@anl.gov>
 * @date
 *
 * @brief
 *
 *
 */
#include "NAME_TO_BE_REPLACED.h"

#include "libmesh/point.h"

template<>
InputParameters validParams<NAME_TO_BE_REPLACED>()
{
  InputParameters params = validParams<InitialCondition>();
  //TODO: add more Param
  //params.addParam<Real>("min", 0.0, "Lower bound of the randomly generated values");
  return params;
}



NAME_TO_BE_REPLACED::NAME_TO_BE_REPLACED(const std::string & name, InputParameters parameters) :
    InitialCondition(name, parameters)
{
}

Real
NAME_TO_BE_REPLACED::value(const Point & /*p*/)
{
  //TODO:
  //return;
}
